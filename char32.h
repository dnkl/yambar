#pragma once

#include <stddef.h>
#include <uchar.h>

size_t c32len(const char32_t *s);
char32_t *ambstoc32(const char *src);
